if (!process.env.apiUrl) {
  console.error("Required `apiUrl` environment variable not set")
  process.exit(1)
}

if (!process.env.appKey) {
  console.error("Required `appKey` environment variable not set")
  process.exit(1)
}

if (!process.env.sessionId) {
  console.error("Required `sessionId` environment variable not set")
  process.exit(1)
}

const { Nuxt, Builder } = require("nuxt")
const express = require("express")
const proxy = require("http-proxy-middleware")
const consola = require("consola")
const config = require("./admin/nuxt.config.js")

const app = express()

app.use(
  "/api",
  proxy(process.env.apiUrl, {
    logLevel: process.env.NODE_ENV === "production" ? "info" : "debug",
    changeOrigin: true
  })
)

app.use(express.static("./dist"))

async function start() {
  const nuxt = new Nuxt(config)

  if (config.dev) {
    await new Builder(nuxt).build()
  } else {
    await nuxt.ready()
  }

  app.use(nuxt.render)

  const host = "0.0.0.0"
  const port = process.env.PORT || 3000

  app.listen(port, host)

  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}

start()
