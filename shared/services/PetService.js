import format from "date-fns/format"

let httpAdapterInstance

export default class PetService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("pets")
  }

  async search(query) {
    const { data: pets } = await httpAdapterInstance.get("search", { query })

    return pets.map(pet => ({ ...pet, birthday: new Date(pet.birthday) }))
  }

  async getById(id) {
    const { data: pet } = await httpAdapterInstance.get(id)

    return { ...pet, birthday: new Date(pet.birthday) }
  }

  async update(pet) {
    const { id } = pet
    const payload = { ...pet, birthday: format(pet.birthday, "YYYY-MM-DD") }

    await httpAdapterInstance.put(id, payload)
  }

  async deleteById(id) {
    await httpAdapterInstance.delete(id)
  }

  async createActivity(petId, activity) {
    const payload = { ...activity, date: format(activity.date, "YYYY-MM-DD") }

    return (await httpAdapterInstance.post(`${petId}/activities`, payload)).data
  }

  async getActivities(petId) {
    return (await httpAdapterInstance.get(`${petId}/activities`)).data
  }
}
