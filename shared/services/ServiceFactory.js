import AxiosHttpAdapter from "./AxiosHttpAdapter"
import InquiryService from "./InquiryService"
import AuthService from "./AuthService"
import ClientService from "./ClientService"
import AppointmentService from "./AppointmentService"
import PetService from "./PetService"
import PurchaseService from "./PurchaseService"
import ServiceService from "./ServiceService"
import ProductService from "./ProductService"
import ActivityService from "./ActivityService"
import PosService from "./PosService"

const defaultConfig = Object.freeze({
  withCredentials: true,
  headers: {
    "Content-Type": "application/json"
  }
})

function createServiceFactory(httpAdapter) {
  const resolved = {}

  return {
    activityService() {
      if (!resolved.activity) {
        resolved.activity = new ActivityService(httpAdapter)
      }

      return resolved.activity
    },

    appointmentService() {
      if (!resolved.appointment) {
        resolved.appointment = new AppointmentService(httpAdapter)
      }

      return resolved.appointment
    },

    authService() {
      if (!resolved.auth) {
        resolved.auth = new AuthService(httpAdapter)
      }

      return resolved.auth
    },

    clientService() {
      if (!resolved.client) {
        resolved.client = new ClientService(httpAdapter)
      }

      return resolved.client
    },

    inquiryService() {
      if (!resolved.inquiry) {
        resolved.inquiry = new InquiryService(httpAdapter)
      }

      return resolved.inquiry
    },

    petService() {
      if (!resolved.pet) {
        resolved.pet = new PetService(httpAdapter)
      }

      return resolved.pet
    },

    posService() {
      if (!resolved.pos) {
        resolved.pos = new PosService(httpAdapter)
      }

      return resolved.pos
    },

    productService() {
      if (!resolved.product) {
        resolved.product = new ProductService(httpAdapter)
      }

      return resolved.product
    },

    purchaseService() {
      if (!resolved.sales) {
        resolved.sales = new PurchaseService(httpAdapter)
      }

      return resolved.sales
    },

    serviceService() {
      if (!resolved.service) {
        resolved.service = new ServiceService(httpAdapter)
      }

      return resolved.service
    },

    getAdapter() {
      return httpAdapter
    }
  }
}

export default {
  createServerDefault(cookie) {
    const baseURL = process.env.apiUrl // We directly call the remote url to avoid extra round-trip
    const headers = { ...defaultConfig.headers }

    if (cookie) {
      headers.Cookie = cookie
    }

    const adapter = new AxiosHttpAdapter({ ...defaultConfig, baseURL, headers })

    return createServiceFactory(adapter)
  },

  createClientDefault() {
    const adapter = new AxiosHttpAdapter({ ...defaultConfig, baseURL: "/api" })

    return createServiceFactory(adapter)
  }
}
