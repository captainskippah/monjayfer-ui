let appointments
let calendar

export default class AppointmentService {
  constructor(httpAdapter) {
    appointments = httpAdapter.appendPath("appointments")
    calendar = httpAdapter.appendPath("calendar")
  }

  async getById(id) {
    return (await appointments.get(id)).data
  }

  async getByMonth(date) {
    const year = date.getFullYear()
    const month = date.getMonth() + 1

    const { data } = await calendar.get(`${year}/${month}`)

    return data.map(appointment => ({
      ...appointment,
      date: new Date(appointment.date)
    }))
  }

  async create(appointment) {
    const { date, ...payload } = appointment

    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()

    try {
      const { data } = await calendar.post(`${year}/${month}/${day}/appointments`, payload)

      return { ...data, date: new Date(data.date) }
    } catch ({ data }) {
      return Promise.reject(new Error(data))
    }
  }

  async delete(id) {
    await appointments.delete(id)
  }

  async cancel(id) {
    await appointments.put(`${id}/cancel`)
  }

  async done(id) {
    await appointments.put(`${id}/done`)
  }

  async schedule(id) {
    await appointments.put(`${id}/schedule`)
  }
}
