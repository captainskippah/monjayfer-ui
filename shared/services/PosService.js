let httpAdapterInstance

export default class PosService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("pos")
  }

  async getSettings() {
    return (await httpAdapterInstance.get("settings")).data
  }

  async checkout(payload) {
    const { items, amount } = payload

    // Our app currently accepts cash payment only
    const newPayload = {
      items,
      payment: {
        amount,
        type: "cash"
      }
    }

    return (await httpAdapterInstance.post("checkout", newPayload)).data
  }
}
