import format from "date-fns/format"

let httpAdapterInstance

export default class ActivityService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("activities")
  }

  async getById(id) {
    return (await httpAdapterInstance.get(id)).data
  }

  async update(activity) {
    const { id } = activity
    const payload = { ...activity, date: format(activity.date, "YYYY-MM-DD") }

    await httpAdapterInstance.put(id, payload)
  }

  async deleteById(id) {
    await httpAdapterInstance.delete(id)
  }
}
