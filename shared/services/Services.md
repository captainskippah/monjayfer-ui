# HTTP Adapter

The common interface for creating requests.

```
{
  get: Promise,
  post: Promise,
  put: Promise,
  delete: Promise
}
```

Adapters should resolve/reject with an object:

```
{
  status: int,
  headers: object,
  data: any
}
```

# Service Factory

The `Service Factory` creates and returns `API Service`s using the `HTTP Adapter` pass to it.

`Service Factory` allows you to configure the `HTTP Adapter` once for  `Server` or `Client` use and request `API Services` from it.

# API Services

An `API Service` contains methods that represents a 1:1 representation of the remote API.

Each method should return a Promise that resolves/rejects only with what is expected from the server
