const axios = require("axios")

export default (function AxiosHttpAdapter() {
  const globalHandlers = {}

  function cleanUrl(url) {
    return url.replace(/(https?:\/\/)|(\/){2,}/g, "$1$2")
  }

  function Constructor(config = {}) {
    let _handlers = {}

    const _instance = axios.create(config)

    function _hasStatusHandler(status) {
      return status in _handlers || status in globalHandlers
    }

    // Extract what is needed only from axios response
    // to be passed on all handlers
    function _extractResponse({ status, headers, data }) {
      return { status, headers, data }
    }

    function _handleStatus(response) {
      const { status } = response
      const callbackParameter = _extractResponse(response)

      if (_hasStatusHandler(status)) {
        const handler = _handlers[status] || globalHandlers[status]
        return handler(callbackParameter)
      }

      return callbackParameter
    }

    async function _handleRequest(request) {
      let result

      try {
        const response = await request
        result = _handleStatus(response)
      } catch (error) {
        // Error from API call
        if (error.response) {
          result = Promise.reject(_handleStatus(error.response))
        }

        // Error from making request
        else {
          console.error(error)
          result = Promise.reject(
            new Error("Sorry, cannot make a request right now")
          )
        }
      }

      // Remove handlers of this request
      _handlers = {}

      return result
    }

    function _clone(newConfig = {}) {
      const newInstance = new Constructor({ ...config, ...newConfig })

      Object.keys(_handlers).forEach(statusCode => {
        newInstance.setStatusHandler(statusCode, _handlers[statusCode])
      })

      return newInstance
    }

    // This will reset appended path
    this.setBasePath = basePath => _clone({ baseURL: cleanUrl(basePath) })

    this.appendPath = path =>
      this.setBasePath(`${config.baseURL || ""}/${path}`)

    this.setGlobalStatusHandler = (status, handler) => {
      globalHandlers[status] = handler

      return this
    }

    this.setStatusHandler = (status, handler) => {
      _handlers[status] = handler

      return this
    }

    this.get = (path, payload) =>
      _handleRequest(_instance.get(path, { params: payload }))

    this.delete = (path, payload) =>
      _handleRequest(_instance.delete(path, { params: payload }))

    this.post = (path, payload) => _handleRequest(_instance.post(path, payload))

    this.put = (path, payload) => _handleRequest(_instance.put(path, payload))
  }

  return Constructor
})()
