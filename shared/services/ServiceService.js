let httpAdapterInstance

export default class ServiceService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("services")
  }

  async getAll() {
    return (await httpAdapterInstance.get("")).data
  }

  async getById(id) {
    return (await httpAdapterInstance.get(id)).data
  }

  async create(service) {
    return (await httpAdapterInstance.post("", service)).data
  }

  async update(service) {
    const { id } = service

    await httpAdapterInstance.put(id, service)
  }

  async deleteById(id) {
    await httpAdapterInstance.delete(id)
  }
}
