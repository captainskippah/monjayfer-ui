let httpAdapterInstance

export default class PurchaseService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("purchases")
  }

  async getAll() {
    const { data } = await httpAdapterInstance.get()

    return data.map(purchase => ({
      ...purchase,
      date: new Date(purchase.date)
    }))
  }

  async getById(id) {
    const { data } = await httpAdapterInstance.get(id)

    return { ...data, date: new Date(data.date) }
  }
}
