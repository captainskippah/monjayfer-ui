let httpAdapterInstance

export default class InquiryService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("inquiries")
  }

  async getAll() {
    return (await httpAdapterInstance.get("")).data
  }

  async create(inquiry) {
    return (await httpAdapterInstance.post("", inquiry)).data
  }

  async markAsRead(inquiry) {
    await httpAdapterInstance.put(`${inquiry}/read`)
  }

  async markAsUnread(inquiry) {
    await httpAdapterInstance.put(`${inquiry}/unread`)
  }
}
