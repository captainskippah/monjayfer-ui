let httpAdapterInstance

export default class ProductService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("products")
  }

  async getAll() {
    return (await httpAdapterInstance.get("")).data
  }

  async getById(id) {
    return (await httpAdapterInstance.get(id)).data
  }

  async create(product) {
    return (await httpAdapterInstance.post("", product)).data
  }

  async update(product) {
    const { id } = product

    await httpAdapterInstance.put(id, product)
  }

  async restock(id, qty) {
    await httpAdapterInstance.post(`${id}/restock`, { qty })
  }

  async consume(id, qty) {
    await httpAdapterInstance.post(`${id}/consume`, { qty })
  }

  async deleteById(id) {
    await httpAdapterInstance.delete(id)
  }
}
