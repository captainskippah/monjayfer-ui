let httpAdapterInstance

export default class AuthService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("auth")
  }

  async login(username, password) {
    // We override the Global 401 handler so we can throw Error
    // instead of committing to Vuex
    const request = httpAdapterInstance.setStatusHandler(401, ({ data }) => {
      throw new Error(data)
    })

    await request.post("login", { username, password })
  }

  async logout() {
    await httpAdapterInstance.post("logout")
  }
}
