import format from "date-fns/format"

let httpAdapterInstance

export default class ClientService {
  constructor(httpAdapter) {
    httpAdapterInstance = httpAdapter.appendPath("clients")
  }

  async getAll() {
    const { data: clients } = await httpAdapterInstance.get("")

    return clients.map(client => ({
      ...client,
      picture: `https://api.adorable.io/avatars/80/${client.id}`
    }))
  }

  async getById(id) {
    const { data: client } = await httpAdapterInstance.get(id)

    return {
      ...client,
      picture: `https://api.adorable.io/avatars/80/${client.id}`
    }
  }

  async getPets(id) {
    const { data: pets } = await httpAdapterInstance.get(`${id}/pets`)

    return pets.map(pet => ({ ...pet, birthday: new Date(pet.birthday) }))
  }

  async create(client) {
    return (await httpAdapterInstance.post("", client)).data
  }

  async createPet(id, pet) {
    const payload = { ...pet, birthday: format(pet.birthday, "YYYY-MM-DD") }

    return (await httpAdapterInstance.post(`${id}/pets`, payload)).data
  }

  async update(client) {
    const { id } = client
    await httpAdapterInstance.put(id, client)
  }

  async deleteById(id) {
    await httpAdapterInstance.delete(id)
  }

  async generateAccount(id) {
    await httpAdapterInstance.post(`${id}/generateAccount`)
  }
}
