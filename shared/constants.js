export const SESSION_NAME = "monjayfer_api_session"

export const APPOINTMENT_SCHEDULED = "scheduled"
export const APPOINTMENT_DONE = "done"
export const APPOINTMENT_CANCELLED = "cancelled"
