module.exports = {
  settings: {
    "import/resolver": {
      nuxt: {
        nuxtSrcDir: "public",
        extensions: ['.js', '.vue']
      }
    }
  }
}
