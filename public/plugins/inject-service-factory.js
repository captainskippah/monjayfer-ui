import ServiceFactory from "../../shared/services/ServiceFactory"

export default function(context, inject) {
  inject("serviceFactory", ServiceFactory.createClientDefault())
}
