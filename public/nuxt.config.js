const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin")

module.exports = {
  srcDir: __dirname,
  buildDir: ".nuxt/public",

  dev: process.env.NODE_ENV !== "production",

  env: {
    apiUrl: process.env.apiUrl,
    recaptcha: process.env.recaptcha
  },

  head: {
    title: "Monjayfer Veterinary Clinic",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui"
      }
    ],
    script: [
      {
        src: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
      },
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-reboot.min.css"
      },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/icon?family=Material+Icons"
      },
      {
        rel: "stylesheet",
        href: "/public/style.css"
      }
    ]
  },

  css: ["vuetify/src/stylus/app.styl"],

  loading: false,

  plugins: ["~~/shared/plugins/vuetify", "~/plugins/inject-service-factory"],

  build: {
    vendor: ["axios", "vuetify"],

    transpile: [/^vuetify/],

    plugins: [new VuetifyLoaderPlugin()],

    extractCSS: true
  }
}
