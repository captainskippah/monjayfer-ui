module.exports =  {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    "airbnb-base",
    "plugin:vue/recommended",
    "plugin:prettier/recommended",
    "prettier/vue"
  ],
  plugins: [
    'prettier',
    'vue'
  ],
  rules: {
    "semi": [2, "never"],
    "no-console": "off",
    "no-unused-vars": "off",
    "no-underscore-dangle": "off",
    "no-param-reassign": [2, { "props": false }],
    "prettier/prettier": ["error", { "semi": false }]
  }
}
