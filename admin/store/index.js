import isAuthenticated from "../check-auth"

export const state = () => ({
  authenticated: false
})

export const mutations = {
  SET_AUTHENTICATED(state, value) {
    state.authenticated = value
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { req }) {
    try {
      commit("SET_AUTHENTICATED", await isAuthenticated(req))
    } catch (error) {
      console.error("nuxtServerInit Error")
      console.error(error)
      commit("SET_AUTHENTICATED", false)
    }
  },

  async login({ commit }, { username, password }) {
    await this.$serviceFactory.authService().login(username, password)
    commit("SET_AUTHENTICATED", true)
  },

  async logout({ commit }) {
    await this.$serviceFactory.authService().logout()
    commit("SET_AUTHENTICATED", false)
  }
}
