export default ({ store, route, redirect }) => {
  if (!store.state.authenticated && route.name !== "login") {
    redirect({ name: "login" })
  } else if (store.state.authenticated && route.name === "login") {
    redirect({ name: "inquiries" })
  }
}
