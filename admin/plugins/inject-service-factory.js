const ServiceFactory = require("../../shared/services/ServiceFactory").default

export default function({ app, store, req, redirect }, inject) {
  let factory

  if (process.server && req.headers.cookie) {
    factory = ServiceFactory.createServerDefault(req.headers.cookie)
  } else {
    factory = ServiceFactory.createClientDefault()
  }

  factory.getAdapter().setGlobalStatusHandler(401, () => {
    store.commit("SET_AUTHENTICATED", false)
    if (process.server) {
      redirect({ name: "login" })
    } else {
      app.router.replace({ name: "login" })
    }
  })

  inject("serviceFactory", factory)
}
