const path = require("path")
const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin")

module.exports = {
  srcDir: __dirname,
  buildDir: ".nuxt/admin",

  dev: process.env.NODE_ENV !== "production",

  env: {
    apiUrl: process.env.apiUrl
  },

  head: {
    title: "Monjayfer Veterinary Clinic",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1, shrink-to-fit=no"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      },
      {
        rel: "stylesheet",
        href:
          "//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,700,400italic|Material+Icons"
      }
    ]
  },

  css: [
    "vuetify/src/stylus/app.styl",
    "~/assets/main.scss"
  ],

  plugins: [
    "~~/shared/plugins/vuetify",
    "~/plugins/inject-service-factory"
  ],

  modules: ["@nuxtjs/style-resources"],

  styleResources: {
    scss: [
      "~~/shared/assets/mixins.scss",
      "~~/shared/assets/breakpoints.scss"
    ]
  },

  loading: false,

  router: {
    base: "/admin/"
  },

  build: {
    vendor: [
      "axios",
      "date-fns",
      "fuse.js",
      "lodash.debounce",
      "lodash.pick",
      "tiptap",
      "tiptap-extensions",
      "vuetify"
    ],

    transpile: [/^vuetify/],

    plugins: [new VuetifyLoaderPlugin()],

    extractCSS: true,

    extend(config, { isDev }) {
      config.node = {
        fs: "empty",
        net: "empty",
        tls: "empty"
      }

      if (isDev) {
        config.devtool = "#eval-source-map"
      }

      if (isDev && process.client) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }
    }
  }
}
