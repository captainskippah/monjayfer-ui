module.exports = {
  settings: {
    "import/resolver": {
      nuxt: {
        nuxtSrcDir: "admin",
        extensions: ['.js', '.vue']
      }
    }
  }
}
