/* eslint-disable global-require */

const cookie = require("cookie")
const { SESSION_NAME } = require("../shared/constants")

function decryptCookie(sessionCookie) {
  const crypto = require("crypto")

  const jsonCookie = JSON.parse(Buffer.from(sessionCookie, "base64"))
  const iv = Buffer.from(jsonCookie.iv, "base64")
  const value = Buffer.from(jsonCookie.value, "base64")
  const key = Buffer.from(process.env.appKey, "base64")

  const decipher = crypto.createDecipheriv("aes-256-cbc", key, iv)

  return Buffer.concat([decipher.update(value), decipher.final()]).toString()
}

function checkIfSessionExists(sessionKey) {
  const redisClient = require("redis").createClient({
    prefix: "monjayfer_api_cache:",
    url: process.env.REDIS_URL
  })

  const { unserialize } = require("php-unserialize")

  return new Promise((resolve, reject) => {
    redisClient.get(sessionKey, (err, reply) => {
      if (err !== null) {
        reject()
      } else if (reply === null) {
        resolve(false)
      } else {
        const sessionData = unserialize(unserialize(reply))
        resolve(process.env.sessionId in sessionData)
      }
    })
  })
}

export default function isAuthenticated(req) {
  if (!req.headers.cookie) {
    return false
  }

  const sessionCookie = cookie.parse(req.headers.cookie)[SESSION_NAME]

  return checkIfSessionExists(decryptCookie(sessionCookie))
}
