# Monjayfer UI

## Environment Variable

Set the following required Environment Variables:

- `apiUrl` - The absolute url to the backend API
- `appKey` - The `APP_KEY` of the backend
- `sessionId` - The name used for identifying if the session is authenticated

Other optional Environment Variables:

- `REDIS_URL` - The redis url of the backend. If none is given, the defaults will be used

## Build Setup

``` bash
# install dependencies
$ yarn install

# generate the static public files
$ yarn run public:generate

# build admin files
$ yarn run admin:build

# launch server
$ yarn start
```
